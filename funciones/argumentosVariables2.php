<?php
    function sumar(...$argumentos){
        $salida=0;
        foreach ($argumentos as $v) {
            $salida+=$v;
        }
        return $salida;
    }
    
    $resultado_de_suma = sumar(20, 7);
    echo "<div>$resultado_de_suma</div>";
    
    $resultado_de_suma = sumar(2, 3, 12, 2);
    echo "<div>$resultado_de_suma</div>";