<?php
    
    // estructura de una funcion
    function sumar_restar($num1, $num2){    //<- creando la misma funcion con sus parametros
        $resultado = [                      //<- creando una array
            'suma' => $num1 + $num2,        //<- creamos cada elemento en el indice de la array 
            'resta' => $num1 - $num2,       
            'producto' => $num1 * $num2,
            'division' => $num1 / $num2
        ];
        return $resultado;                  //<- siempre necesario en una funcion, xq devuelve, asignas con nombre lo q devuelve
    }
    
    // llamamos para ejecutar la funcion (necesitqa 2 numeros num1 y num2)
    $resultado= sumar_restar(20, 7);
    
    //mostramos en pantalla con un FOREACH
        foreach ($resultado as $operacion => $valor) {
            echo "<div>$operacion: $valor</div>";
        }
            
?>