<?php
// creacion de funcion (siempre arriba del documento

// con return (RECOMENDABLE)
function miFuncion1(){
    $salida="";
    $salida="Yo soy";
    $salida="<br>";
    $salida="Tú eres tú";
    
    return $salida;
}



// sin return xo muestra con echo para mostrar (NO RECOMENDABLE xq las funciones son xa operar)
function miFuncion(){
    echo "Yo soy yo";
    echo "<br>";
    echo "Tú eres tú y tus consecuencias";
    echo "<br>";
    echo "Nosotros somos tú y yo<br>";
}

// llamamos la funcion (q primero hemos creado)
miFuncion();
miFuncion();
miFuncion();

// ahora muestras la funcion con echo
echo miFuncion1();
?>